package com.example.matthew.chess;

import java.util.ArrayList;
/**
 * Subclass of Piece contains legal moves for a bishop
 * @author Matthew Eder & Alex Weinrich
 */
public class Bishop extends Piece {
	/**
	 * A list of positions a piece would travel through in a valid move  
	 */
	ArrayList<Position>  path = new ArrayList<Position>();
	/**
	 * Calls super class Piece with player param
	 * @param player that owns the piece
	 */
	public Bishop(Player player) {
		super(player);
	}
	/**
	 * @return the path the piece would take on a valid move in an ArrayList of positions
	 */
	public ArrayList<Position> getPath() {	
		return path;
	}	
	/**
	 * Calls Piece isValid first with same params
	 * @param board Board object of the game
 	 * @param fromX the initial x position of the piece
 	 * @param fromY the initial y position of the piece
 	 * @param toX the x position the user inputs
 	 * @param toY the y position the user inputs
	 * @return true if the move is a valid chess move for a bishop
	 */
	public boolean isValid(Board board, int fromX, int fromY , int toX, int toY) {
		if(!super.isValid(board, fromX, fromY, toX, toY)) {
			return false;
		}
		path.clear();
		// move only one space diagonally 
		if( (fromX + 1 == toX && fromY + 1 == toY) ||  (fromX - 1 == toX && fromY - 1 == toY) ||
		    (fromX + 1 == toX && fromY - 1 == toY) ||  (fromX - 1 == toX && fromY + 1 == toY) ) {
			return true;
		}
		
		// moves multiple spaces
        if(Math.abs(toX - fromX) == Math.abs(toY - fromY)) {
        	// moved up
        	if(toX > fromX) {
        		// move up and right
        		if(toY > fromY){
        			for(int i = 1; i < toY - fromY; i++) {
						if(board.getPosition(fromX+i,fromY+i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX+i,fromY+i));
					}	
            		return true;
            	// move up and left
        		} else {
        			for(int i = 1; i < fromY - toY; i++) {
						if(board.getPosition(fromX+i,fromY-i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX+i,fromY-i));
					}	
            		return true;
        		}
        	// move down
        	} else {
        		// move down and right
        		if(toY > fromY){
        			for(int i = 1; i < toY - fromY; i++) {
						if(board.getPosition(fromX-i,fromY+i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX-i,fromY+i));
					}	
            		return true;
            	// move down and left
        		} else {
        			for(int i = 1; i < fromY - toY; i++) {
						if(board.getPosition(fromX-i,fromY-i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX-i,fromY-i));
					}	
            		return true;
        		}
        	}
        }
		path.clear();
		return false;
	} 
	/**
	 * Calls super toString first to get the string associated with the player
	 * @return prints the bishop as shown in project description "Player + 'B'" 
	 */
    public String toString() {
    	return super.toString() + "b";
    }
}
