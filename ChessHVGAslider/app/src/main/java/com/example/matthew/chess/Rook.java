package com.example.matthew.chess;

import java.util.ArrayList;
/**
 * Subclass of Piece contains legal moves for a rook
 * @author Matthew Eder & Alex Weinrich
 */
public class Rook extends Piece {
	/**
	 * Is true if piece has moved used for castling
	 */
    boolean hasMoved = false;
    /**
	 * A list of positions a piece would travel through in a valid move  
	 */
	ArrayList<Position>  path = new ArrayList<Position>();
	/**
	 * Calls super class Piece with player param
	 * @param player that owns the piece
	 */
	public Rook(Player player) {
		super(player);
	}
	/**
	 * @return the path the piece would take on a valid move in an ArrayList of positions
	 */
	public ArrayList<Position> getPath() {
		return path;
	}
	/**
	 * @return hasMoved field true if it has moved
	 */
    public boolean getHasMoved() {
        return hasMoved;
    }
    /**
	 * Calls Piece isValid first with same params
	 * @param board Board object of the game
 	 * @param fromX the initial x position of the piece
 	 * @param fromY the initial y position of the piece
 	 * @param toX the x position the user inputs
 	 * @param toY the y position the user inputs
	 * @return true if the move is a valid chess move for a rook
	 */
	public boolean isValid(Board board, int fromX, int fromY , int toX, int toY) {
		if(!super.isValid(board, fromX, fromY, toX, toY)) {
			return false;
		}
		if ((((fromX!=0 && fromX!=7) || (fromY!=7)) && (board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())) || 
			(((fromX!=0 && fromX!=7) || (fromY!=0)) && (!board.getPosition(fromX, fromY).getPiece().getPlayer().getColor()))) {
				hasMoved = true;
		}
		
		path.clear();
				
		// moved vertically
		if(fromX == toX) {
			if (Math.abs(fromY - toY) != 1) {
				// moved down
				if(fromY > toY) {
					for(int i = fromY-1; i > toY; i--) {
						if(board.getPosition(toX,i).isOccupied()) {
							return false;
						}
						path.add(board.getPosition(toX,i)); 
					}	
				// moved up
				} else {
					for(int i = fromY+1; i < toY; i++) {
						if(board.getPosition(toX,i).isOccupied()) {
							return false;
						}
						path.add(board.getPosition(toX,i)); 
					}
				}
			}
            return true;
		}
		// moved horizontally
		else if(fromY == toY) {
        	if (Math.abs(fromX - toX) != 1) { 
        		// moved left
				if(fromX > toX) {
					for(int i = fromX-1; i > toX; i--) {
						if(board.getPosition(i,toY).isOccupied()) {
							return false;
						}
						path.add(board.getPosition(i,toY)); 
					}	
				// moved right
				} else {
					for(int i = fromX+1; i < toX; i++) {
						if(board.getPosition(i,toY).isOccupied()) {
							return false;
						}
						path.add(board.getPosition(i,toY)); 
					}
				}
			}
            return true;
        } else {
        	return false;
        }
	}
	/**
	 * Calls super toString first to get the string associated with the player
	 * @return prints the bishop as shown in project description "Player + 'R'"
	 */
    public String toString() {
    	return super.toString() + "r";
    }
}
