package com.example.matthew.chess;

import java.util.ArrayList;
/**
 * A representation of a piece on the chess board and is the super 
 * class to the types of pieces in chess.
 * @author Matthew Eder & Alex Weinrich
 */
public class Piece {
	/**
	 * A player object that owns the piece
	 */
 	Player player;
 	/**
 	 * A list of positions a piece would travel through, needed for the 
 	 * rook, bishop, queen subclasses
 	 */
	private ArrayList<Position>  path = new ArrayList<Position>();
	/**
	 * The constructor of the Piece class, sets Player field to the player param  
	 * @param player
	 */
 	public Piece(Player player) {
         this.player = player;
    }
 	/**
 	 * @return the path of the piece needed for rook, bishop and queen
 	 */
 	public ArrayList<Position> getPath() {	
		return path;
	}
 	/**
 	 * @return player that owns the piece
 	 */
 	public Player getPlayer() {
 		return player;
 	}
 	/**
 	 * Checks conditions that would lead to a move being invalid for
 	 * any given chess pieces
 	 * @param board Board object of the game
 	 * @param fromX the initial x position of the piece
 	 * @param fromY the initial y position of the piece
 	 * @param toX the x position the user inputs
 	 * @param toY the y position the user inputs
 	 * @return boolean if the move is valid chess move or not
 	 */
	public boolean isValid(Board board, int fromX, int fromY , int toX, int toY) {
    	Check c = new Check();
    	Piece startPiece = board.getPosition(fromX, fromY).getPiece();
    	Piece endPiece = board.getPosition(toX, toY).getPiece();
    	//check if same spot
		if(fromX == toX && fromY == toY) {
			return false;
		} 

		// check if end location is piece player owns
		else if(board.getPosition(toX, toY).isOccupied() && 
			   (board.getPosition(toX, toY).getPiece().getPlayer() == 
				board.getPosition(fromX, fromY).getPiece().getPlayer())) {
			return false;
		}
		// check if piece causes check to own check
		else if(c.inCheck(board, fromX, fromY, toX, toY)) {
			board.getPosition(fromX, fromY).setPosition(startPiece);
			board.getPosition(toX, toY).setPosition(endPiece);
			return false;
		}
		// Piece not owned by player
		try {
			 if(board.getPosition(fromX, fromY).isOccupied() &&
						board.getPosition(fromX, fromY).getPiece().getPlayer().getColor() == 
						board.getPosition(toX, toY).getPiece().getPlayer().getColor()){
				 return false;
			 }
		} catch(Exception e) {
			
		}
		return true;
 	}
 	/**
 	 * @return color of that corresponds to the color of the player field
 	 */
 	public String toString() {
 		 if(player.getColor())
    		 return "w";
    	 else
    		 return "b";
 	}

}
