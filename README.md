# README #
**IMPORTANT**
- only tested on 3.2 inch HVGA slider (ADP1) (320 x 480: mdpi) 

Extras

- stalemate

known issues

- Enpassant(in general(says is illegal sometimes) and undo)

- Promotion (no dialogue window, some pawns cant reach end(sometimes))

- undo AI move (found and fixed after due date)

After late updates

- fixed enpassant

- ported castling 

- ported promotion

- fixed ports to draw

- fixed undo for everything

- ai promotion

- sorting in reverse orders

- non case sensitive sorting

- two sort buttons in action bar for date and abc

- in game text display improvements

- nexus 5x version (phone I own)

- stuff to added for any screen size compatibility 

to do list

- single player

- unlimited undos

- more screen size compatibility